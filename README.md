# About project

ART is integration tools between jira, gitlab and slack,
all you need is including pipeline and configuring `.art.yml` file in repository.

# Features

* Configuring process for development in points `in progress/review/ready to test/ready to release` between jira and gitlab
* Automatically creating releases
* Release process control in points `update release/do release/close release`
* Supporting for semver tag format
* Slack notifications
* Automatically building and publishing docker image

# Pipeline

* [Install](../../wikis/Pipeline/Install)
* [Variables](../../wikis/Pipeline/Variables)
* [Branching model](../../wikis/Branching-model)

## Jobs
* [setup](../../wikis/Jobs/setup)
* [in progress](../wikis/Jobs/in progress)
* [review](../../wikis/Jobs/review)
* [ready to test](../wikis/Jobs/ready to test)
* [ready to release](../wikis/Jobs/ready to release)
* [create release](../wikis/Jobs/create release)
* [update release](../wikis/Jobs/update release)
* [do release](../wikis/Jobs/do release)
* [close release](../wikis/Jobs/close release)
* [create image](../wikis/Jobs/create image)

# ART file
* [About file](../wikis/ART-file/About file)
* [Additional features](../wikis/ART-file/Additional features)

# Yaml DSL

## Settings
* [settings](../wikis/Yaml DSL/Settings)
* [settings.daemon](../wikis/Yaml DSL/Settings%3A%3Adaemon)
* [settings.repository](../wikis/Yaml DSL/Settings%3A%3Arepository)
* [settings.integrations](../wikis/Yaml DSL/Settings%3A%3Aintegrations)

## Development
* [development](../wikis/Yaml DSL/Development)
* [development.in_progress](../wikis/Yaml-DSL/Development%3A%3Ain progress)
* [development.review](../wikis/Yaml-DSL/Development%3A%3Areview)
* [development.ready_to_test](../wikis/Yaml-DSL/Development%3A%3Aredy to test)
* [development.ready_to_release](../wikis/Yaml-DSL/Development%3A%3Aredy to release) 

## Release
* [release](../wikis/Yaml DSL/Release)
* [release.create](../wikis/Yaml-DSL/Release%3A%3Acreate)
* [release.update](../wikis/Yaml-DSL/Release%3A%3Aupdate)
* [release.do](../wikis/Yaml-DSL/Release%3A%3Ado)
* [release.close](../wikis/Yaml-DSL/Release%3A%3Aclose)

# Git commands
* [to review](../wikis/Git commands/To review)
* [merge request target branch](../wikis/Git commands/Merge request target branch)
